build_test_app_android:
	cd app_test; \
	mkdir ./app_test/build; \
	rm ./app_test/build/magic_painter_test.apk; \
	unity -batchmode -quit -projectPath ./app/ -executeMethod MagicPainter.CLI.TestBuilder.Android -logFile -; \

deploy_test_app_android:
	adb install -r ./app_test/build/magic_painter_test.apk; \
	adb shell am start -n com.nomad.MagicPainter/com.unity3d.player.UnityPlayerActivity; \
	adb forward tcp:13000 tcp:13000; \

run_tests_android:
	cd ./app_test/Test; \
	echo "==>Restore test project"; \
	dotnet restore; \
	echo "==>Run tests"; \
	dotnet test -v n; \

build_sketch_data:
	@docker build -t tf-lite-sketches -f ml/Dockerfile .
	@if [ ! -d "./ml/build" ]; then mkdir ml/build; else rm -f -r ml/build; fi
	@docker run --rm -v $(CURDIR)/ml/build:/mnt/out tf-lite-sketches bash -c '\
		python sketch_transform.py && \
		python sketch_training.py && \
		cp data/sketches.tflite /mnt/out'
	
build_tf_lite_base:
	@docker build -t tf-lite-base ./tf_lite_lib

build_tf_lite_android: build_tf_lite_base
	@docker build -t tf-lite-android ./tf_lite_lib/android
	@if [ ! -d "./tf_lite_lib/android/build" ]; then mkdir tf_lite_lib/android/build; else rm -f -r tf_lite_lib/android/build; fi
	@docker run --rm -v $(CURDIR)/tf_lite_lib/android/build:/mnt/out tf-lite-android bash -c '\
		cd workspace/tensorflow && \
		bazel build -c opt --config=android_arm64 --verbose_failures --define tflite_with_xnnpack=true //tensorflow/lite/c:libtensorflowlite_c.so && \
		# bazel build -c opt --config=android_arm64 //tensorflow/lite/delegates/nnapi:nnapi_delegate && \
		bazel build -c opt --config=android_arm64 --verbose_failures --copt -Os --copt -DTFLITE_GPU_BINARY_RELEASE --copt -fvisibility=hidden --linkopt -s --strip always //tensorflow/lite/delegates/gpu:libtensorflowlite_gpu_delegate.so && \
		bazel build -c opt --config=android_arm64 --verbose_failures --copt -Os --copt -DTFLITE_GPU_BINARY_RELEASE --copt -fvisibility=hidden --linkopt -s --strip always //tensorflow/lite/delegates/gpu:libtensorflowlite_gpu_gl.so && \
		cd bazel-bin/tensorflow/lite && \
		cp c/libtensorflowlite_c.so /mnt/out && \
		# cp delegates/nnapi/libnnapi_delegate.so /mnt/out && \
		cp delegates/gpu/libtensorflowlite_gpu_delegate.so /mnt/out && \
		cp delegates/gpu/libtensorflowlite_gpu_gl.so /mnt/out'

build_tf_lite_linux: build_tf_lite_base
	@docker build -t tf-lite-linux ./tf_lite_lib/linux
	@if [ ! -d "./tf_lite_lib/linux/build" ]; then mkdir tf_lite_lib/linux/build; else rm -f -r tf_lite_lib/linux/build; fi
	@docker run --rm -v $(CURDIR)/tf_lite_lib/linux/build:/mnt/out tf-lite-linux bash -c '\
		cd workspace/tensorflow && \
		bazel build -c opt --verbose_failures --define tflite_with_xnnpack=true tensorflow/lite/c:tensorflowlite_c && \
		cp bazel-bin/tensorflow/lite/c/libtensorflowlite_c.so /mnt/out'	 

build_tf_lite_windows: build_tf_lite_base
	@if [ ! -d "./tf_lite_lib/windows/build" ]; then mkdir tf_lite_lib/windows/build; else rm -rf tf_lite_lib/windows/build; fi
	@docker run --rm -v $(CURDIR)/tf_lite_lib/windows/build:/mnt/out tf-lite-base bash -c '\
		cd workspace && \
		rsync --info=progress2 -a tensorflow /mnt/out'
	@cp tf_lite_lib/windows/.tf_configure.bazelrc tf_lite_lib/windows/build/tensorflow
	@cd tf_lite_lib/windows/build/tensorflow && \
		echo "" >> .tf_configure.bazelrc && \
		echo build --action_env PYTHON_BIN_PATH=\""$(shell python -c 'import sys;print(sys.executable.replace("\\", "/"))')\"" >> .tf_configure.bazelrc && \
		echo build --action_env PYTHON_LIB_PATH=\""$(shell python -c 'import site;print(site.getsitepackages()[1].replace("\\", "/"))')\"" >> .tf_configure.bazelrc && \
		echo build --python_path=\""$(shell python -c 'import sys;print(sys.executable.replace("\\", "/"))')\"" >> .tf_configure.bazelrc
	@cd tf_lite_lib/windows/build/tensorflow && \
		bazel build -c opt --define tflite_with_xnnpack=true tensorflow/lite/c:tensorflowlite_c && \
		cp bazel-bin/tensorflow/lite/c/tensorflowlite_c.dll ../
	@rm -rf tf_lite_lib/windows/build/tensorflow

build_tf_lite_macos: build_tf_lite_base
	@if [ ! -d "./tf_lite_lib/macos/build" ]; then mkdir tf_lite_lib/macos/build; else rm -rf tf_lite_lib/macos/build; fi
	@docker run --rm -v $(CURDIR)/tf_lite_lib/macos/build:/mnt/out tf-lite-base bash -c '\
		cd workspace && \
		rsync --info=progress2 -a tensorflow /mnt/out'
	@cp tf_lite_lib/macos/.tf_configure.bazelrc tf_lite_lib/macos/build/tensorflow
	@cd tf_lite_lib/macos/build/tensorflow && \
		echo "" >> .tf_configure.bazelrc && \
		echo build --action_env PYTHON_BIN_PATH=\""$(shell python -c 'import sys;print(sys.executable.replace("\\", "/"))')\"" >> .tf_configure.bazelrc && \
		echo build --action_env PYTHON_LIB_PATH=\""$(shell python -c 'import site;print(site.getsitepackages()[0].replace("\\", "/"))')\"" >> .tf_configure.bazelrc && \
		echo build --python_path=\""$(shell python -c 'import sys;print(sys.executable.replace("\\", "/"))')\"" >> .tf_configure.bazelrc
	@cd tf_lite_lib/macos/build/tensorflow && \
		bazel build -c opt --config=macos --define tflite_with_xnnpack=true tensorflow/lite/c:tensorflowlite_c && \
		cp bazel-bin/tensorflow/lite/c/libtensorflowlite_c.dylib ../
	@sed -i "" "s/\"cpu\": \"darwin\",/\"cpu\": \"darwin_x86_64\",/" tf_lite_lib/macos/build/tensorflow/third_party/cpuinfo/BUILD.bazel
	# Metal Delegate
    # v2.3.0 or later, Need to apply the following patch to build metal delegate
    # For further info
    # https://github.com/tensorflow/tensorflow/issues/41039#issuecomment-664701908
	@cd tf_lite_lib/macos/build/tensorflow && \
		bazel build -c opt --config=macos --copt -Os --copt -DTFLITE_GPU_BINARY_RELEASE --copt -fvisibility=default --linkopt -s --strip always --apple_platform_type=macos //tensorflow/lite/delegates/gpu:tensorflow_lite_gpu_dylib && \
		cp bazel-bin/tensorflow/lite/delegates/gpu/tensorflow_lite_gpu_dylib.dylib ../libtensorflowlite_metal_delegate.dylib
	@sed -i "" "s/\"cpu\": \"darwin_x86_64\",/\"cpu\": \"darwin\",/" tf_lite_lib/macos/build/tensorflow/third_party/cpuinfo/BUILD.bazel
	@rm -rf tf_lite_lib/macos/build/tensorflow

APPLE_BAZEL_FILES:=tensorflow/lite/ios/BUILD \
	tensorflow/lite/objc/BUILD \
	tensorflow/lite/swift/BUILD \
	tensorflow/lite/tools/benchmark/experimental/ios/BUILD

IOS_FILES:=tensorflow/lite/objc/TensorFlowLiteObjC.podspec \
	tensorflow/lite/swift/TensorFlowLiteSwift.podspec

build_tf_lite_ios: build_tf_lite_base
	@if [ ! -d "./tf_lite_lib/ios/build" ]; then mkdir tf_lite_lib/ios/build; else rm -rf tf_lite_lib/ios/build; fi
	@docker run --rm -v $(CURDIR)/tf_lite_lib/ios/build:/mnt/out tf-lite-base bash -c '\
		cd workspace && \
		rsync --info=progress2 -a tensorflow /mnt/out'
	@cp tf_lite_lib/ios/.tf_configure.bazelrc tf_lite_lib/ios/build/tensorflow
	@cd tf_lite_lib/ios/build/tensorflow && \
		echo "" >> .tf_configure.bazelrc && \
		echo build --action_env PYTHON_BIN_PATH=\""$(shell python -c 'import sys;print(sys.executable.replace("\\", "/"))')\"" >> .tf_configure.bazelrc && \
		echo build --action_env PYTHON_LIB_PATH=\""$(shell python -c 'import site;print(site.getsitepackages()[0].replace("\\", "/"))')\"" >> .tf_configure.bazelrc && \
		echo build --python_path=\""$(shell python -c 'import sys;print(sys.executable.replace("\\", "/"))')\"" >> .tf_configure.bazelrc
	@for f in $(APPLE_BAZEL_FILES); do \
		ln -sf "$(CURDIR)/tf_lite_lib/ios/build/tensorflow/$$f.apple" "$(CURDIR)/tf_lite_lib/ios/build/tensorflow/$$f"; \
	done
	@for f in $(IOS_FILES); do \
		ln -sf "$(CURDIR)/tf_lite_lib/ios/build/tensorflow/$$f" "$(CURDIR)/tf_lite_lib/ios/build/tensorflow/$(basename $f)"; \
	done
	@cd tf_lite_lib/ios/build/tensorflow && \
		bazel build -c opt --config=ios_fat //tensorflow/lite/ios:TensorFlowLiteC_framework && \
		bazel build -c opt --config=ios_fat //tensorflow/lite/ios:TensorFlowLiteCMetal_framework && \
    	bazel build -c opt --config=ios_fat //tensorflow/lite/ios:TensorFlowLiteCCoreML_framework && \
     	bazel build -c opt --config=ios --ios_multi_cpus=armv7,arm64,x86_64 //tensorflow/lite/ios:TensorFlowLiteSelectTfOps_framework
	@cd tf_lite_lib/ios/build/tensorflow && \
		unzip bazel-bin/tensorflow/lite/ios/TensorFlowLiteC_framework.zip -d ../ && \
		unzip bazel-bin/tensorflow/lite/ios/TensorFlowLiteCMetal_framework.zip -d ../ && \
    	unzip bazel-bin/tensorflow/lite/ios/TensorFlowLiteCCoreML_framework.zip -d ../ && \
    	unzip bazel-bin/tensorflow/lite/ios/TensorFlowLiteSelectTfOps_framework.zip -d ../
	@rm -rf tf_lite_lib/ios/build/tensorflow
	