namespace Test.Pages
{
    public class BasePage
    {
        protected AltUnityDriver Driver { get; set; }
        
        protected BasePage(AltUnityDriver driver)
        {
            Driver = driver;
        }
    }
}