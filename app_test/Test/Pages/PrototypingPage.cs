namespace Test.Pages
{
    public class PrototypingPage : BasePage
    {
        public PrototypingPage(AltUnityDriver driver) : base(driver)
        {
        }

        public AltUnityObject PaintCanvas => Driver.FindObject(By.COMPONENT, "PaintCanvas");
        public AltUnityObject ResetButton => Driver.FindObject(By.NAME, "ResetCanvasButton");
        public AltUnityObject ClassifyButton => Driver.FindObject(By.NAME, "ClassifyButton");

        public AltUnityObject FPSCounter =>
            Driver.FindObject(By.COMPONENT, "MagicPainter.Diagnostics.FPSCounter");

        public string CanvasWidth => PaintCanvas.GetComponentProperty("MagicPainter.PaintCanvas", "canvasWidth");
        public string CanvasHeight => PaintCanvas.GetComponentProperty("MagicPainter.PaintCanvas", "canvasHeight");

        public string ClassifyRequestTime => Driver.FindObject(By.NAME, "DebugOutTFReqTime")
            .GetComponentProperty("MagicPainter.Diagnostics.DebugPrinter", "PropertyValue");

        public string FPSRecordStart() =>
            FPSCounter.CallComponentMethod("MagicPainter.Diagnostics.FPSCounter", "StartFPSRecord", "");

        public string FPSRecordStop() =>
            FPSCounter.CallComponentMethod("MagicPainter.Diagnostics.FPSCounter", "StopFPSRecord", "");

        public string ClassifyDrawing() =>
            PaintCanvas.CallComponentMethod("MagicPainter.PaintCanvas", "ClassifyDrawing", "");

        public string ResetCanvas() => PaintCanvas.CallComponentMethod("MagicPainter.PaintCanvas", "ResetCanvas", "");

        public bool IsDisplayed()
        {
            if (ResetButton != null && ClassifyButton != null && PaintCanvas != null && FPSCounter != null)
                return true;
            return false;
        }
    }
}