using System;
using NUnit.Framework;

namespace Test
{
    public class BaseTest
    {
        protected AltUnityDriver Driver { get; set; }

        [OneTimeSetUp]
        public void BaseSetup()
        {
            Driver = new AltUnityDriver();
        }

        [OneTimeTearDown]
        public void BaseTearDown()
        {
            Driver.Stop();
        }
    }
}