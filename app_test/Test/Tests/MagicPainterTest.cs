using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text;
using System.Threading;
using Assets.AltUnityTester.AltUnityDriver.UnityStruct;
using CsvHelper;
using NUnit.Framework;
using Test.Pages;

namespace Test
{
    public class MagicPainterTest : BaseTest
    {
        private const int Iterations = 50;

        private List<float> fpsValuesIdle = new List<float>();
        private List<float> fpsValuesDrawing = new List<float>();
        private List<float> fpsValuesClassify = new List<float>();
        private List<float> reqTimeValuesClassify = new List<float>();

        private PrototypingPage _prototypingPage;

        [OneTimeSetUp]
        public void SetUp()
        {
            _prototypingPage = new PrototypingPage(Driver);
        }

        [OneTimeTearDown]
        public void TearDown()
        {
            _prototypingPage.ResetCanvas();
            WriteResultsToCSV();
        }

        [Test]
        public void Test_01_PrototypingScene()
        {
            Assert.True(_prototypingPage.IsDisplayed());
            Assert.AreEqual("PaintCanvas", _prototypingPage.PaintCanvas.name);
        }

        [Test]
        public void Test_02_IdlePerformance()
        {
            _prototypingPage.ResetCanvas();
            for (int i = 0; i < Iterations; i++)
            {
                _prototypingPage.FPSRecordStart();
                Thread.Sleep(3000);
                string fpsRes = _prototypingPage.FPSRecordStop();
                float fps = float.Parse(fpsRes);
                Assert.NotZero(fps);
                fpsValuesIdle.Add(fps);
            }
        }

        [Test]
        public void Test_03_DrawingPerformance()
        {
            string w = _prototypingPage.CanvasWidth;
            string h = _prototypingPage.CanvasHeight;

            Assert.NotNull(w);
            Assert.NotNull(h);

            float canvasWidth = float.Parse(w);
            float canvasHeight = float.Parse(h);

            Assert.NotZero(canvasWidth);
            Assert.NotZero(canvasHeight);

            for (int i = 0; i < Iterations; i++)
            {
                _prototypingPage.FPSRecordStart();
                DrawRandomTriangle(canvasWidth, canvasHeight);
                string outFPS = _prototypingPage.FPSRecordStop();
                Assert.NotNull(outFPS);
                var fps = float.Parse(outFPS);
                Assert.NotZero(fps);
                fpsValuesDrawing.Add(fps);
                _prototypingPage.ResetCanvas();
            }
        }

        [Test]
        public void Test_04_ClassifyPerformanceAndTime()
        {
            string w = _prototypingPage.CanvasWidth;
            string h = _prototypingPage.CanvasHeight;

            Assert.NotNull(w);
            Assert.NotNull(h);
            float canvasWidth = float.Parse(w);
            float canvasHeight = float.Parse(h);
            Assert.NotZero(canvasWidth);
            Assert.NotZero(canvasHeight);

            for (int i = 0; i < Iterations; i++)
            {
                DrawRandomTriangle(canvasWidth, canvasHeight);
                _prototypingPage.FPSRecordStart();
                Thread.Sleep(500);
                _prototypingPage.ClassifyDrawing();
                Thread.Sleep(500);
                string outFPS = _prototypingPage.FPSRecordStop();
                Assert.NotNull(outFPS);
                var fps = float.Parse(outFPS);
                Assert.NotZero(fps);
                fpsValuesClassify.Add(fps);

                string outReqTime = _prototypingPage.ClassifyRequestTime;
                Assert.NotNull(outReqTime);
                var reqTime = float.Parse(outReqTime);
                Assert.NotZero(reqTime);
                reqTimeValuesClassify.Add(reqTime);
                _prototypingPage.ResetCanvas();
            }
        }

        private void DrawRandomTriangle(float canvasWidth, float canvasHeight)
        {
            var random = new Random();
            const int range = 50;
            const float margin = 0.1f;
            var left = (int) (canvasWidth * margin);
            var right = (int) (canvasWidth * (1 - margin));
            var bottom = (int) (canvasHeight * margin);
            var top = (int) (canvasHeight * (1 - margin));
            var centerX = (int) (canvasWidth / 2);
            var startPoint = new AltUnityVector2(random.Next(left, left + range), random.Next(bottom, bottom + range));
            var positions = new[]
            {
                startPoint,
                new AltUnityVector2(random.Next(centerX - (range / 2), centerX + (range / 2)),
                    random.Next(top - range, top)),
                new AltUnityVector2(random.Next(right - range, right),
                    random.Next(bottom, bottom + range)),
                startPoint
            };
            Driver.TapScreen(startPoint.x, startPoint.y);
            for (int i = 0; i < positions.Length - 1; i++)
            {
                Driver.SwipeAndWait(positions[i], positions[i + 1], 0.1f);
            }
        }

        private void WriteResultsToCSV()
        {
            const string outDir = "../../../../out";
            if (!Directory.Exists(outDir))
                Directory.CreateDirectory(outDir);

            using (var writer = new StreamWriter($"{outDir}/results.csv", false, Encoding.UTF8))
            using (var csvWriter = new CsvWriter(writer, CultureInfo.InvariantCulture))
            {
                csvWriter.Configuration.Delimiter = ";";
                csvWriter.WriteHeader<ResultRecord>();
                csvWriter.NextRecord();
                for (int i = 0; i < Iterations; i++)
                {
                    var record = new ResultRecord
                    {
                        Iteration = i,
                        FPSIdle = fpsValuesIdle[i], FPSDrawing = fpsValuesDrawing[i],
                        FPSClassify = fpsValuesClassify[i], RequestTime = reqTimeValuesClassify[i]
                    };
                    csvWriter.WriteRecord(record);
                    csvWriter.NextRecord();
                }

                writer.Flush();
            }
        }
    }

    class ResultRecord
    {
        public int Iteration { get; set; }
        public float FPSIdle { get; set; }
        public float FPSDrawing { get; set; }
        public float FPSClassify { get; set; }
        public float RequestTime { get; set; }
    }
}