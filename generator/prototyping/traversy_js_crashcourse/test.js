// OOP in ES5
function Person(firstName, lastName, dob){
    this.firstName = firstName;
    this.lastName = lastName;
    this.dob = new Date(dob);
}

// Add functions to prototype so it won't be shown with the entire object
// in console, but you can still call it
Person.prototype.getFullName = function(){
    return `${this.firstName} ${this.lastName}`;
}
Person.prototype.getBirthYear = function() {
    return this.dob.getFullYear();
}

const person1 = new Person("John", "Doe", "4-3-1980");

console.log(person1);
console.log(person1.getFullName());

// OOP in ES6, same thing as above, just prettier
class BetterPerson {
    constructor(firstName, lastName, dob) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.dob = new Date(dob);
    }

    getBirthYear = function() {
        return this.dob.getFullYear();
    }

    getFullName = function(){
        return `${this.firstName} ${this.lastName}`;
    }
}

const person2 = new BetterPerson("Mary", "Smith", "3-6-1970");
console.log(person2);
