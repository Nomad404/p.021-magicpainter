// Single element
// console.log(document.getElementById("my-form"));
// console.log(document.querySelector("h1"));

// Multiple elements
// console.log(document.querySelectorAll(".item"));
// console.log(document.getElementsByClassName("item")); // avoid this
// console.log(document.getElementsByTagName("li"));

// const items = document.querySelectorAll(".item");
// items.forEach((item) => console.log(item));

// const ul = document.querySelector(".items");
// ul.remove();
// ul.firstElementChild.textContent = "Hello";

const btn = document.querySelector(".btn");
btn.addEventListener("click", (e) => {
    e.preventDefault();

});
