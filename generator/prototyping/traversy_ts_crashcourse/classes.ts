interface UserInt {
    name: string;
    email: string;
    age: number;
    register();
    payInvoice();
}

class User implements UserInt{
    name: string;
    email: string;
    age: number;

    constructor(name: string, email: string, age: number) {
        this.name = name;
        this.email = email;
        this.age = age;
    }

    register(){
        console.log(`${this.name} is now registered`);
    }

    payInvoice() {
        console.log(`${this.name} paid his invoice`);
    }
}

let john = new User("John", "email", 23);

john.register();

class ProUser extends User {
    id: number;

    constructor(id: number, name: string, email: string, age: number) {
        super(name, email, age);
        this.id = id;
    }
}
