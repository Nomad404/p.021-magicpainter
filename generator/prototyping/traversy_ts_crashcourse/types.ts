let myString: string;
let myNum: number;
let myBool: boolean;
let myVar: any;
let strArr: string[];

myString = "Hello world";
myNum = 22;
myBool = true;
myVar = 5;

let strArray: Array<string>;
let numArr: Array<number>;
let boolArr: Array<boolean>;

