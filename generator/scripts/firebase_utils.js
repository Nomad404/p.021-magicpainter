module.exports = function () {
    const utils = {},
        fetch = require("node-fetch");

    // Signs user into firebase to retrieve idToken
    utils.signIntoFirebase = async function (apiKey, email, password) {
        console.log(`signing into Firebase with ${email}`);
        let address = "https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword";
        address += `?key=${apiKey}&email=${email}&password=${password}&returnSecureToken=true`
        let res = await fetch(address, {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            }
        });
        return await res.json();
    }

    const rootFirestoreAddress = "https://firestore.googleapis.com/v1/";

    // Generates a base path
    utils.getBaseFirestorePath = function (cloudProjectName) {
        return `${rootFirestoreAddress}projects/${cloudProjectName}/databases/(default)/documents`;
    }

    // Fetches data from a Firestore collection
    utils.getCollectionDocsPaged = async function (firebaseProjectName, idToken, collectionPath, nextPageToken, maxPageSize) {
        let address = `${rootFirestoreAddress}${collectionPath}?`;
        if (nextPageToken !== undefined) {
            address += `pageToken=${nextPageToken}`;
        }
        if (maxPageSize !== undefined) {
            address += `&pageSize=${maxPageSize}`;
        }
        let res = await fetch(address, {
            headers: {
                "Authorization": `Bearer ${idToken}`
            }
        });
        return await res.json();
    }

    // Gets all documents from a collection
    utils.getAllCollectionDocs = async function (cloudProjectName, collectionPath, idToken) {
        console.log("retrieving all docs from: " + collectionPath);
        let data = [];

        async function getNextCollectionPage(nextPageToken) {
            const res = await utils.getCollectionDocsPaged(cloudProjectName, idToken, collectionPath, nextPageToken);
            if (!res.error) {
                let docs = res.documents;
                if (docs.length > 0) {
                    data = data.concat(docs);
                }
                return res.nextPageToken;
            } else {
                console.log(res.error);
                return undefined;
            }
        }

        try {
            let nextPageToken = await getNextCollectionPage();
            while (nextPageToken !== undefined) {
                nextPageToken = await getNextCollectionPage(nextPageToken);
            }
        } catch (error) {
            console.log(error);
        }
        return data;
    }

    utils.getFirestoreDocIDs = async function (cloudProjectName, collectionPath, idToken) {
        const address = `${utils.getBaseFirestorePath(cloudProjectName)}${collectionPath}`;
        return await fetch(address, {
            method: "GET",
            headers: {
                "Authorization": `Bearer ${idToken}`,
            },
            redirect: "follow"
        }).then(res => res.json())
            .then(json => json.documents.map(function (doc) {
                return utils.getDocIdFromPath(doc.name);
            }));
    }

    utils.getAllFinishedSampleIDs = async function (cloudProjectName, idToken) {
        console.log("getting IDs of all finished samples");
        const address = `${utils.getBaseFirestorePath(cloudProjectName)}:runQuery`;
        const query = {
            "structuredQuery": {
                "from": [
                    {
                        "collectionId": "samples"
                    }
                ],
                "where": {
                    "fieldFilter": {
                        "field": {
                            "fieldPath": "finished"
                        },
                        "op": "EQUAL",
                        "value": {
                            "booleanValue": true
                        }
                    }
                }
            }
        }
        const res = await fetch(address, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${idToken}`,
            },
            redirect: "follow",
            body: JSON.stringify(query),
        });
        return (await res.json()).map(function (obj) {
            return obj.document.name;
        });
    }

    // Retrieves a document id from a path
    utils.getDocIdFromPath = function (pathName) {
        return pathName.slice(pathName.lastIndexOf("/") + 1);
    }

    return utils;
};
