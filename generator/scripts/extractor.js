const FirebaseUtils = require("./firebase_utils")();
const fsPromises = require("fs").promises;
const sharp = require("sharp");

(async function () {
    const secrets = JSON.parse(await fsPromises.readFile("../secrets/firebase.json"));

    const cloudName = secrets.cloudName;
    const cloudAPIKey = secrets.browserAPIKey;
    const accEmail = secrets.clientAccEmail;
    const accPw = secrets.clientAccPassword;

    const idToken = (await FirebaseUtils.signIntoFirebase(cloudAPIKey, accEmail, accPw)).idToken;
    const docPaths = await FirebaseUtils.getAllFinishedSampleIDs(cloudName, idToken);
    const samples = await Promise.all(docPaths.map(async (docPath) => {
        const storedSampleSketches = await FirebaseUtils.getAllCollectionDocs(cloudName, `${docPath}/sketches`, idToken);
        return {
            sampleID: FirebaseUtils.getDocIdFromPath(docPath),
            sketches: storedSampleSketches.map(doc => {
                const sketch = doc.fields;
                const symbol = sketch.symbol.mapValue.fields;
                return {
                    index: symbol.index.integerValue,
                    category: symbol.category.stringValue,
                    name: symbol.name.stringValue,
                    data: sketch.data.stringValue,
                    canvasWidth: sketch.canvasWidth.integerValue,
                    canvasHeight: sketch.canvasHeight.integerValue,
                }
            })
        };
    }));
    console.log(`received ${samples.length} samples`);
    await fsPromises.mkdir(`./out/sketches/`, {recursive: true});
    for (sample of samples) {

        const sampleID = sample.sampleID;
        console.log(`saving sketches of ${sampleID}`);
        await Promise.all(sample.sketches.map(async (sketch) => {
            const encodedImage = sketch.data.split(";base64,").pop();
            const fileName = `out/sketches/${sampleID}.${sketch.index}.${sketch.category}.${sketch.name}.png`;

            await fsPromises.writeFile(fileName, encodedImage, {encoding: "base64"});
            let buffer = await sharp(fileName)
                .trim()
                .toBuffer();
            const trimmedFileInfo = await sharp(buffer)
                .toFile(fileName);
            const trimmedWidth = trimmedFileInfo.width;
            const trimmedHeight = trimmedFileInfo.height;
            const newSize = trimmedWidth > trimmedHeight ? trimmedWidth : trimmedHeight;

            const paddingTopBottom = Math.round(((newSize * 1.3) - trimmedHeight) / 2);
            const paddingLeftRight = Math.round(((newSize * 1.3) - trimmedWidth) / 2);
            buffer = await sharp(buffer)
                .extend({
                    top: paddingTopBottom,
                    bottom: paddingTopBottom,
                    left: paddingLeftRight,
                    right: paddingLeftRight,
                    background: {r: 0, g: 0, b: 0, alpha: 0}
                })
                .toBuffer();
            await sharp(buffer)
                .resize(28)
                .flatten({background: "#ffffff"})
                .negate()
                .toFile(fileName);
        }));
    }
}());
