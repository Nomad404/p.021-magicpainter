import Vue from "vue"
import VueRouter, {RouteConfig} from "vue-router"
import Start from "@/pages/Start.vue"

Vue.use(VueRouter)

const routes: Array<RouteConfig> = [
    {
        path: "/",
        name: "Start",
        component: Start
    },
    {
        path: "/about",
        name: "About",
        // route level code-splitting
        // this generates a separate chunk (about.[hash].js) for this route
        // which is lazy-loaded when the route is visited.
        component: () => import(/* webpackChunkName: "about" */ "@/pages/About.vue")
    },
    {
        path: "/sample",
        name: "Sample",
        component: () => import("@/pages/Session.vue")
    },
    {
        path: "/symbols",
        name: "Symbols Overview",
        component: () => import("@/pages/SymbolsOverview.vue")
    }
]

const router = new VueRouter({
    routes
})

export default router
