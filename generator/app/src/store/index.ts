import Vue from "vue"
import Vuex from "vuex"
import sessionModule from "@/store/modules/session"
import {templates} from "@/data/symbolTemplate"
import {SymbolTemplate, symViewProps} from "@/data/symbolTemplate"

Vue.use(Vuex)

export default new Vuex.Store<RootState>({
    state: {
        showAppBar: true,
        symbolsVersion: 0,
        symbols: templates,
        symbolProps: symViewProps,
        showSnackbar: false,
        snackBarText: "",
        showLoading: false,
    },
    mutations: {},
    actions: {
        async showSnackbar({state}, message: string) {
            state.snackBarText = message;
            state.showSnackbar = true;
        },
        async setLoading({state}, value: boolean) {
            state.showLoading = value;
        }
    },
    getters: {},
    modules: {
        session: sessionModule,
    },
})

export interface RootState {
    showAppBar: boolean;
    symbolsVersion: number;
    symbols: SymbolTemplate[];
    symbolProps: any;
    showSnackbar: boolean;
    snackBarText: string;
    showLoading: boolean;
}
