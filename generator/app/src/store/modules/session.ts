import {Module} from "vuex";
import {SymbolTemplate} from "@/data/symbolTemplate";
import {RootState} from "@/store";

const sessionModule: Module<SessionState, RootState> = {
    namespaced: true,
    state: () => ({
        sampleID: "",
        sketches: [],
        samplerLoading: false,
    }),
    mutations: {
        setSampleLoading(state, value: boolean) {
            state.samplerLoading = value;
        },
        updateSketchData(state, payload) {
            const symbol: SymbolTemplate = payload.symbol;
            state.sketches[payload.sessionSymbolIndex] = {
                symbol: symbol,
                canvasWidth: payload.canvasWidth,
                canvasHeight: payload.canvasHeight,
                data: payload.data
            };
        },
        updateSampleID(state, payload) {
            if (typeof payload === "string" && payload !== "") {
                state.sampleID = payload;
            } else {
                throw new Error("payload not a string")
            }
        },
        reset(state) {
            state.sampleID = "";
            state.sketches = [];
            state.samplerLoading = false;
        },
    },
    actions: {
        async submitSketchData({commit, state, dispatch}, payload) {
            await commit("updateSketchData", payload);
            return await dispatch("saveSketchesToFirestore");
        },
        async saveSketchesToFirestore({state}) {
            const url = new URL("https://europe-west3-magicpainter-9b747.cloudfunctions.net/submitSample");
            url.search = new URLSearchParams({
                id: state.sampleID,
            }).toString();

            return await fetch(url.toString(), {
                method: "POST",
                body: JSON.stringify(state.sketches)
            });
        },
        async finishSample({state}) {
            const url = new URL("https://europe-west3-magicpainter-9b747.cloudfunctions.net/finishSample");
            url.search = new URLSearchParams({
                id: state.sampleID,
            }).toString();
            return await fetch(url.toString(), {
                method: "POST",
            });
        },
        async isSampleFinished({state}) {
            const url = new URL("https://europe-west3-magicpainter-9b747.cloudfunctions.net/isSampleFinished");
            url.search = new URLSearchParams({
                id: state.sampleID
            }).toString();
            return await fetch(url.toString(), {
                method: "GET",
            });
        },
        async getSavedSampleData({state}, symbolName: string) {
            const url = new URL("https://europe-west3-magicpainter-9b747.cloudfunctions.net/getSampleData");
            url.search = new URLSearchParams({
                id: state.sampleID,
                name: symbolName,
            }).toString();

            return await fetch(url.toString(), {
                method: "GET",
            });
        }
    },
    getters: {
        hasUndefinedSketches(state) {
            console.log(state.sketches);
            for (const sketch of state.sketches) {
                if (sketch === undefined) {
                    return true;
                }
            }
            return false;
        }
    }
};

export interface SessionState {
    sampleID: string;
    sketches: object[];
    samplerLoading: boolean;
}

export default sessionModule;
