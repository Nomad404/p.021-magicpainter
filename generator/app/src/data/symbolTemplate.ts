export const symViewProps = {
    viewWidth: 60,
    viewHeight: 60,
    strokeWidth: 5,
};

export const symbols = [
    {
        name: "triangle.up_even",
        paths: ["M30 5 L5 55 L55 55 Z"]
    },
    {
        name: "triangle.down_even",
        paths: ["M30 55 L5 5 L55 5 Z"]
    },
    {
        name: "triangle.left_even",
        paths: ["M 5 30 L55 5 L 55 55 Z"]
    },
    {
        name: "triangle.right_even",
        paths: ["M55 30 L5 5 L5 55 Z"]
    },
    {
        name: "triangle.up_left",
        paths: ["M5 5 L55 5 L55 55 Z"]
    },
    {
        name: "triangle.up_right",
        paths: ["M5 5 L55 5 L5 55 Z"]
    },
    {
        name: "triangle.down_left",
        paths: ["M5 55 L55 55 L5 5 Z"]
    },
    {
        name: "triangle.down_right",
        paths: ["M55 55 L55 5 L5 55 Z"]
    },

    {
        name: "square.default",
        paths: ["M5 5 L5 55 L55 55 L55 5 Z"],
    },
    {
        name: "square.half_left",
        paths: ["M45 10 L15 10 L15 50 L45 50"]
    },
    {
        name: "square.half_right",
        paths: ["M15 10 L45 10 L45 50 L15 50"]
    },
    {
        name: "square.half_top",
        paths: ["M10 45 L10 15 L50 15 L50 45"]
    },
    {
        name: "square.half_bottom",
        paths: ["M10 15 L10 45 L50 45 L50 15"]
    },
    {
        name: "square.quart_top_left",
        paths: ["M45 15 L15 15 L15 45"]
    },
    {
        name: "square.quart_top_right",
        paths: ["M15 15 L45 15 L45 45"]
    },
    {
        name: "square.quart_bottom_left",
        paths: ["M15 15 L15 45 L45 45"]
    },
    {
        name: "square.quart_bottom_right",
        paths: ["M15 45 L45 45 L45 15"]
    },

    {
        name: "circle.default",
        paths: ["M 3 30 a 12 12 0 1 1 54,0 a 27 27 0 1 1 -54,0"],
    },
    {
        name: "circle.half_left",
        paths: ["M 30 5 a 15 15 0 1 0 0 50"]
    },
    {
        name: "circle.half_right",
        paths: ["M 30 5 a 15 15 0 1 1 0 50"]
    },
    {
        name: "circle.half_top",
        paths: ["M5 30 a 15 15 0 1 1 50 0"]
    },
    {
        name: "circle.half_bottom",
        paths: ["M5 30 a 15 15 0 1 0 50 0"]
    },
    {
        name: "circle.quart_top_left",
        paths: ["m 45 15 a 25 25 0 0 0 -25 25"]
    },
    {
        name: "circle.quart_top_right",
        paths: ["m 15 15 a 25 25 0 0 1 25 25"]
    },
    {
        name: "circle.quart_bottom_left",
        paths: ["m 15 15 a 25 25 0 0 0 25 25"]
    },
    {
        name: "circle.quart_bottom_right",
        paths: ["m 45 15 a 25 25 0 0 1 -25 25"]
    },

    {
        name: "arrow.up_even",
        paths: ["M5,55 L30,5 L55,55"]
    },
    {
        name: "arrow.down_even",
        paths: ["M5,5 L30,55 L55,5"]
    },
    {
        name: "arrow.left_even",
        paths: ["M55,5 L5,30 L55,55"]
    },
    {
        name: "arrow.right_even",
        paths: ["M5,5 L55,30 L5,55"]
    },

    {
        name: "line.vertical",
        paths: ["M30,5 L30,55"]
    },
    {
        name: "line.horizontal",
        paths: ["M5,30 L55,30"]
    },
    {
        name: "line.dia_tl_br",
        paths: ["M5,5 L55,55"]
    },
    {
        name: "line.dia_tr_bl",
        paths: ["M55,5 L5,55"]
    },
];

export class SymbolTemplate {

    index: number;
    category: string;
    name: string;
    viewWidth: number;
    viewHeight: number;
    paths: string[];

    constructor(index: number, viewWidth: number, viewHeight: number, paths: string[]) {
        this.index = index;
        const indexValues = symbols[this.index].name.split(".");
        this.category = indexValues[0];
        this.name = indexValues[1];
        this.viewWidth = viewWidth;
        this.viewHeight = viewHeight;
        this.paths = paths;
    }
}

export const templates = symbols.map((obj, index) => {
    if(obj.paths === undefined) throw new Error("symbol has no paths");
    return new SymbolTemplate(index, symViewProps.viewWidth, symViewProps.viewHeight, obj.paths);
});
