module.exports = {
  "transpileDependencies": [
    "vuetify"
  ],

  pages: {
    index: {
      entry: "src/main.ts",
      title: "Sketch sampler"
    }
  },

  pwa: {
    name: "Sketch sampler"
  },

  pluginOptions: {
    i18n: {
      locale: 'en',
      fallbackLocale: 'en',
      localeDir: 'locales',
      enableInSFC: false
    }
  }
}
