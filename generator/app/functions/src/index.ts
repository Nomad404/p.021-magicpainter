import * as functions from "firebase-functions";
import * as admin from "firebase-admin";
import * as os from "os";
import * as path from "path";

const sharp = require("sharp");
const fs = require("fs");
const logger = functions.logger;

admin.initializeApp(functions.config())

export const createSample = functions
    .region(`europe-west3`)
    .https.onRequest(async (req, res) => {
        res.set("Access-Control-Allow-Origin", "*");
        res.set("Access-Control-Allow-Methods", "GET");
        res.set("Access-Control-Allow-Headers", "Content-Type");
        try {
            const symbolsVersion = req.query.symbolsVersion as string;
            const db = admin.firestore();
            const docRef = await db.collection("samples")
                .add({
                    finished: false,
                    createdAt: new Date(Date.now()),
                    symbolsVersion: symbolsVersion,
                });
            console.log(`Sample created with ID ${docRef.id}`);
            res.status(201).send({
                docID: docRef.id,
            });
        } catch (error) {
            console.log(error);
            res.status(500).send(error);
        }
    })

export const submitSample = functions
    .region("europe-west3")
    .https.onRequest(async (req, res) => {
        res.set("Access-Control-Allow-Origin", "*");
        res.set("Access-Control-Allow-Methods", "POST");
        res.set("Access-Control-Allow-Headers", "Content-Type");
        try {
            const sampleID = req.query.id as string;
            const sketches: any[] = JSON.parse(req.body);
            if (sampleID && sampleID !== "" && sketches) {
                const db = admin.firestore();
                for (const sketch of sketches) {
                    if (sketch) {
                        const sketchName = `${sketch.symbol.index}.${sketch.symbol.category}.${sketch.symbol.name}`;
                        await db.collection("samples")
                            .doc(sampleID).collection("sketches")
                            .doc(sketchName).set(sketch);
                    }
                }
                res.sendStatus(200);
            } else {
                res.status(400).send("missing sampleID or sketches");
            }
        } catch (error) {
            console.log(error);
            res.status(500).send(error);
        }
    })

export const finishSample = functions
    .region("europe-west3")
    .https.onRequest(async (req, res) => {
        res.set("Access-Control-Allow-Origin", "*");
        res.set("Access-Control-Allow-Methods", "POST");
        res.set("Access-Control-Allow-Headers", "Content-Type");
        try {
            const sampleID = req.query.id as string;
            if (sampleID && sampleID !== "") {
                const db = admin.firestore();
                const docRef = await db.collection("samples")
                    .doc(sampleID);
                const snapshot = await docRef.get();
                if (snapshot.exists) {
                    await docRef.update({"finished": true, "finishedAt": new Date(Date.now())});
                    res.sendStatus(200);
                } else {
                    res.sendStatus(404);
                }
            } else {
                res.sendStatus(400);
            }
        } catch (error) {
            console.log(error);
            res.status(500).send(error);
        }
    })

export const getSampleData = functions
    .region("europe-west3")
    .https.onRequest(async (req, res) => {
        res.set("Access-Control-Allow-Origin", "*");
        res.set("Access-Control-Allow-Methods", "GET");
        res.set("Access-Control-Allow-Headers", "Content-Type");
        try {
            const sampleID = req.query.id as string;
            const name = req.query.name as string;
            if (sampleID && sampleID !== "" && name && name !== "") {
                const db = admin.firestore();
                const snapshot = await db.collection("samples")
                    .doc(sampleID).collection("sketches")
                    .doc(name).get();
                if (snapshot.exists) {
                    res.status(200).send(JSON.stringify(snapshot.data()));
                } else {
                    res.sendStatus(404);
                }
            } else {
                res.sendStatus(400);
            }
        } catch (error) {
            console.log(error);
            res.status(500).send(error);
        }
    })

export const isSampleFinished = functions
    .region("europe-west3")
    .https.onRequest(async (req, res) => {
        res.set("Access-Control-Allow-Origin", "*");
        res.set("Access-Control-Allow-Methods", "GET");
        res.set("Access-Control-Allow-Headers", "Content-Type");
        try {
            const sampleID = req.query.id as string;
            if (sampleID && sampleID !== "") {
                const db = admin.firestore();
                const docRef = await db.collection("samples")
                    .doc(sampleID);
                const snapshot = await docRef.get();
                if (snapshot.exists) {
                    const value = await snapshot.get("finished");
                    res.status(200).send(value);
                } else {
                    res.sendStatus(404);
                }
            } else {
                res.sendStatus(400);
            }
        } catch (error) {
            console.log(error);
            res.status(500).send(error);
        }
    })

export const collectAllSamples = functions
    .runWith({
        timeoutSeconds: 540,
        memory: "512MB",
    })
    .region("europe-west3")
    .pubsub.schedule("*/9 * * * *")
    .timeZone("Europe/Berlin")
    .onRun(async (context) => {
        const db = admin.firestore();
        const finishedSamplesCol = await db.collection("samples").where("finished", "==", true).get();
        logger.log(`${finishedSamplesCol.size} samples collected`);
        for(const sample of finishedSamplesCol.docs){
            const sampleID = sample.id;
            const sampleSketches = await sample.ref.collection("sketches").get();
            for(const sampleSketch of sampleSketches.docs){
                const fileName = path.join(os.tmpdir(), `${sampleID}.${sampleSketch.id}.png`);
                const encodedImage = sampleSketch.get("data").split(";base64,").pop();
                let buffer = Buffer.from(encodedImage, "base64");
                buffer = await sharp(buffer)
                    .trim()
                    .toBuffer();
                const trimmedFileInfo = await sharp(buffer)
                    .toFile(fileName);
                const trimmedWidth = trimmedFileInfo.width;
                const trimmedHeight = trimmedFileInfo.height;
                const newSize = trimmedWidth > trimmedHeight ? trimmedWidth : trimmedHeight;

                const paddingTopBottom = Math.round(((newSize * 1.3) - trimmedHeight) / 2);
                const paddingLeftRight = Math.round(((newSize * 1.3) - trimmedWidth) / 2);

                buffer = await sharp(buffer)
                    .extend({
                        top: paddingTopBottom,
                        bottom: paddingTopBottom,
                        left: paddingLeftRight,
                        right: paddingLeftRight,
                        background: {r: 0, g: 0, b: 0, alpha: 0}
                    })
                    .toBuffer();
                await sharp(buffer)
                    .resize(28)
                    .flatten({background: "#FFFFFF"})
                    .negate()
                    .toFile(fileName);

            }
            const files = await fs.promises.readdir(os.tmpdir());
            logger.log(`${files.length} images created`);
        }
    })
