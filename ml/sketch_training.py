import numpy as np
import tensorflow as tf
from tensorflow.keras.layers import Dense, Conv2D, Dropout, Flatten, MaxPool2D
from tensorflow.keras.losses import SparseCategoricalCrossentropy
from tensorflow.keras.models import Sequential

# load data from file
with np.load("data/sketches.npz", allow_pickle=True) as f:
	x_train, y_train = f['x_train'], f['y_train']
# x_test, y_test = f['x_test'], f['y_test']

x_train = x_train.reshape(len(x_train), 28, 28, 1)
# x_test = x_test.reshape(len(x_test), 28, 28, 1)

# Normalize data
x_train = x_train / 255
# x_test = x_test / 255

# Train CNN
model = Sequential([
	Conv2D(28, kernel_size=(3, 3), input_shape=x_train[0].shape),
	MaxPool2D(pool_size=(2, 2)),
	Flatten(),
	Dense(128, activation="relu"),
	Dropout(0.2),
	Dense(34, activation="softmax")
])

loss_fn = SparseCategoricalCrossentropy(from_logits=True)

model.compile(optimizer="adam", loss=loss_fn, metrics=["accuracy"])
model.fit(x=x_train, y=y_train, epochs=10)

# print("model accuracy: ", model.evaluate(x_test, y_test, verbose=False)[1])

print("export model as tfLite model file")
converter = tf.lite.TFLiteConverter.from_keras_model(model)
tflite_model = converter.convert()
with tf.io.gfile.GFile("data/sketches.tflite", "wb") as f:
	f.write(tflite_model)
