from os import walk, mkdir, path

import numpy as np
from PIL import Image
from numpy import asarray

print("getting all sketch filenames")
baseSketchPath = "sketches/"
fileNames = []
for (_, _, names) in walk(baseSketchPath):
	fileNames.extend(names)
	break

imageData = []
sketchIndices = []

print("transforming data into numpy arrays")
for fileName in fileNames:
	segments = fileName.split(".")
	index = int(segments[1])
	sketchIndices.append(index)
	imageData.append(asarray(Image.open(baseSketchPath + fileName).convert("L")))

x_train = np.array(imageData)
y_train = np.array(sketchIndices)

# In case you want to try to have a test set
# x_train = np.array(imageData[:len(imageData)//2])
# y_train = np.array(sketchIndices[:len(sketchIndices)//2])

# x_test = np.array(imageData[len(imageData)//2:])
# y_test = np.array(sketchIndices[len(sketchIndices)//2:])

print("creating output path")
if not path.exists("data"):
	mkdir("data")

print("saving data to data/sketches.npz")
np.savez("data/sketches.npz", x_train=x_train, y_train=y_train)
