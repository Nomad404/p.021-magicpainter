﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class PostEffectScript : MonoBehaviour
{
    public Material material;

    void OnRenderImage(RenderTexture src, RenderTexture dest) {
        // src is the full rendered scene that you would normally
        // send directly to the monitor. We are intercepting
        // this so we can do a bit more work, before passing it on<Project Sdk="Microsoft.NET.Sdk">
        
        // // PRETENDING TO DO IMAGE EFFECT IN CPU
        // Color[] pixels = new Color[1920 * 1080];

        // for(int x = 0; x < 1920; x++){
        //     for(int y = 00; y < 1080; y++){
        //         pixels[x + y * 1080].r = Mathf.Pow(2.18f, 3.17f);
        //     }
        // }

        Graphics.Blit(src, dest, material);
    }
}
