﻿using System;
using Altom.Editor;
using UnityEditor;
using UnityEngine;

namespace MagicPainter.CLI
{
    public class TestBuilder
    {
        static void Android()
        {
            try
            {
                var levels = new[] {"Assets/MagicPainter/Scenes/Prototyping.unity"};
                var outPath = "../app_test/build/magic_painter_test.apk";
                var options = new BuildPlayerOptions
                {
                    scenes = levels,
                    locationPathName = outPath,
                    target = BuildTarget.Android,
                    options = BuildOptions.Development
                };

                AltUnityBuilder.AddAltUnityTesterInScritpingDefineSymbolsGroup(BuildTargetGroup.Android);
                AltUnityBuilder.InsertAltUnityInScene(levels[0]);
                var result = BuildPipeline.BuildPlayer(options);
                AltUnityBuilder.RemoveAltUnityTesterFromScriptingDefineSymbols(BuildTargetGroup.Android);
            }
            catch (Exception ex)
            {
                Debug.LogException(ex);
            }
        }
    }
}