using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TensorFlowLite;
using TensorFlowLite.Utils;
using UnityEngine;
using UnityEngine.UI;

namespace MagicPainter.Scripts
{
    public class SketchInterpreter : MonoBehaviour
    {
        private const int SketchInputSize = 28;
        private const int SketchTypeNum = 34;

        [SerializeField, FilePopup("*.tflite")]
        private string fileName = "sketches.tflite";

        [SerializeField] private Text outText;
        [SerializeField] private ComputeShader computeShader;

        private bool _isProcessing;
        private readonly float[,] _inputs = new float[SketchInputSize, SketchInputSize];
        private readonly float[] _outputs = new float[SketchTypeNum];

        private int _kernelIndex;
        private ComputeBuffer _inputBuffer;
        private RenderTexture _inputRenderTex;

        private Interpreter _interpreter;
        
        private readonly StringBuilder _stringBuilder = new StringBuilder();

        void Start()
        {
            _inputRenderTex = new RenderTexture(28, 28, 0, RenderTextureFormat.ARGB32);

            var options = new InterpreterOptions()
            {
                threads = 2,
                useNNAPI = true,
            };
            _interpreter = new Interpreter(FileUtil.LoadFile(fileName), options);
            // _interpreter.ResizeInputTensor(0, new[] {1, 28, 28, 1});
            _interpreter.AllocateTensors();

            _kernelIndex = computeShader.FindKernel("TextureToBuffer");
            _inputBuffer = new ComputeBuffer(SketchInputSize * SketchInputSize, sizeof(float));
        }

        void OnDestroy()
        {
            _interpreter?.Dispose();
            _inputBuffer?.Dispose();
        }

        public void OnDrawTexture(RenderTexture texture)
        {
            ResizeInputTexture(texture);
            if (!_isProcessing)
            {
                Invoke(_inputRenderTex);
            }
        }

        private void ResizeInputTexture(RenderTexture original)
        {
            RenderTexture.active = _inputRenderTex;
            Graphics.Blit(original, _inputRenderTex);
            RenderTexture.active = null;
        }

        private void Invoke(RenderTexture inputTexture)
        {
            _isProcessing = true;

            computeShader.SetTexture(_kernelIndex, "InputTexture", inputTexture);
            computeShader.SetBuffer(_kernelIndex, "OutputBuffer", _inputBuffer);
            computeShader.SetInt("ImageSize", SketchInputSize);
            computeShader.Dispatch(_kernelIndex, SketchInputSize / 4, SketchInputSize / 4, 1);
            _inputBuffer.GetData(_inputs);

            float startTime = Time.realtimeSinceStartup;
            _interpreter.SetInputTensorData(0, _inputs);
            _interpreter.Invoke();
            _interpreter.GetOutputTensorData(0, _outputs);
            float duration = Time.realtimeSinceStartup - startTime;
            
            _stringBuilder.Clear();
            _stringBuilder.AppendLine($"Process time: {duration: 0.00000} sec");
            _stringBuilder.AppendLine("---");
            for (int i = 0; i < _outputs.Length; i++)
            {
                _stringBuilder.AppendLine($"{i}: {_outputs[i]: 0.00}");
            }
            outText.text = _stringBuilder.ToString();
            
            _isProcessing = false;
        }
    }
}