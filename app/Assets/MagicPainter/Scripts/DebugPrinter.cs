﻿using UnityEngine;
using UnityEngine.UI;

namespace MagicPainter.Diagnostics
{
    [RequireComponent(typeof(Text))]
    public class DebugPrinter : MonoBehaviour
    {
        public string propertyName = "";
        private string _propertyValue = "";
        private Text uiText;

        void Awake()
        {
            uiText = GetComponent<Text>();
        }

        public string PropertyValue
        {
            get => _propertyValue;
            set
            {
                _propertyValue = value;
                uiText.text = _propertyValue == "" ? "" : $"{propertyName}={_propertyValue}";
            }
        }
    }
}