﻿using System.Globalization;
using UnityEngine;

namespace MagicPainter.Diagnostics
{
    public class FPSCounter : MonoBehaviour
    {
        public DebugPrinter printer;

        private float _currentFPS;

        private bool _isRecording;
        private int _fpsRecordCounter;
        private float _fpsRecordAvg;

        void Update()
        {
            _currentFPS = 1.0f / Time.unscaledDeltaTime;
            printer.PropertyValue = ((int) _currentFPS).ToString("G", CultureInfo.InvariantCulture);
            if (_isRecording)
            {
                UpdateCumulativeAvgFPS(_currentFPS);
            }
        }

        public void StartFPSRecord()
        {
            _fpsRecordCounter = 0;
            _fpsRecordAvg = 0;
            _isRecording = true;
        }

        public float StopFPSRecord()
        {
            _isRecording = false;
            return _fpsRecordAvg;
        }

        private void UpdateCumulativeAvgFPS(float newFPS)
        {
            ++_fpsRecordCounter;
            _fpsRecordAvg += (newFPS - _fpsRecordAvg) / _fpsRecordCounter;
        }
    }
}