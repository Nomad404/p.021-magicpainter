﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace MagicPainter
{
    [RequireComponent(typeof(RawImage))]
    public class PaintCanvas : MonoBehaviour
    {
        // public Material penMat;
        public Texture2D penTexture;

        private readonly float _penScale = 1f;
        private Color _penColor = Color.white;
        private const float LerpDrawDamp = 0.02f;

        private RawImage _rawImage;
        private RectTransform _canvasRect;

        public Material CanvasMat
        {
            get => _rawImage.material;
        }

        public Color CanvasColor
        {
            get => CanvasMat.GetColor(MaterialParams.Color);
            set => CanvasMat.SetColor(MaterialParams.Color, value);
        }

        [HideInInspector] public float canvasWidth;
        [HideInInspector] public float canvasHeight;

        private RenderTexture _renderTex;

        private Vector2 _currentMousePos;
        private Vector2 _lastMousePos;

        private Rect _uvRect = new Rect(0, 0, 1, 1);

        [SerializeField] private UnityEvent<RenderTexture> onDrawTexture = new UnityEvent<RenderTexture>();

        void Start()
        {
            SetupCanvas();
            ResetCanvas();
        }

        void Update()
        {
            if (Input.GetMouseButtonDown(0))
            {
                if (RectTransformUtility.ScreenPointToLocalPointInRectangle(_canvasRect, Input.mousePosition, null,
                    out _currentMousePos))
                {
                    DrawOnClick(_currentMousePos);
                }
            }

            if (Input.GetMouseButton(0))
            {
                if (RectTransformUtility.ScreenPointToLocalPointInRectangle(_canvasRect, Input.mousePosition, null,
                    out _currentMousePos))
                {
                    DrawOnDrag(_currentMousePos);
                }
            }

            if (Input.GetMouseButtonUp(0))
            {
                EndDraw();
            }
        }

        private void SetupCanvas()
        {
            _rawImage = GetComponent<RawImage>();
            _canvasRect = GetComponent<RectTransform>();

            var rect = _canvasRect.rect;
            canvasWidth = rect.width;
            canvasHeight = rect.height;

            _renderTex = new RenderTexture((int)canvasWidth, (int)canvasHeight, 0, RenderTextureFormat.ARGB32)
            {
                filterMode = FilterMode.Bilinear,
                useMipMap = false
            };

            _rawImage.texture = _renderTex;
            CanvasMat.mainTexture = _renderTex;

            // penMat.mainTexture = penTexture;
        }

        private void DrawOnClick(Vector3 mousePos)
        {
            Vector3 uvPos = Rect.PointToNormalized(_canvasRect.rect, mousePos);
            if (!_uvRect.Contains(uvPos)) return;
            // penMat.color = penColor;
            var scaledUVPos = new Vector3(uvPos.x * canvasWidth, canvasHeight - uvPos.y * canvasHeight,
                0f);
            _lastMousePos = scaledUVPos;
            var brushWidth = penTexture.width * _penScale;
            var brushHeight = penTexture.height * _penScale;
            var rect = new Rect(scaledUVPos.x - brushWidth * 0.5f, scaledUVPos.y - brushHeight * 0.5f, brushWidth,
                brushHeight);
            GL.PushMatrix();
            GL.LoadPixelMatrix(0, _renderTex.width, _renderTex.height, 0);
            RenderTexture.active = _renderTex;
            // Graphics.DrawTexture(rect, penTexture, penMat);
            Graphics.DrawTexture(rect, penTexture);
            RenderTexture.active = null;
            GL.PopMatrix();
            onDrawTexture.Invoke(_renderTex);
        }

        void DrawOnDrag(Vector2 mousePos)
        {
            if (mousePos == _lastMousePos) return;
            Vector3 uvPos = Rect.PointToNormalized(_canvasRect.rect, mousePos);
            var scaledUVPos = new Vector3(uvPos.x * canvasWidth, canvasHeight - uvPos.y * canvasHeight, 0);
            // penMat.color = penColor;
            GL.PushMatrix();
            GL.LoadPixelMatrix(0, _renderTex.width, _renderTex.height, 0);
            RenderTexture.active = _renderTex;
            LerpDraw(scaledUVPos, _lastMousePos);
            RenderTexture.active = null;
            GL.PopMatrix();
            _lastMousePos = scaledUVPos;
            onDrawTexture.Invoke(_renderTex);
        }

        private void LerpDraw(Vector3 current, Vector3 prev)
        {
            var distance = Vector2.Distance(current, prev);
            if (!(distance > 0f)) return;
            var tempPos = Vector2.zero;
            var scaledPenWidth = penTexture.width * _penScale;
            var scaledPenHeight = penTexture.height * _penScale;
            var lerpDamp = Mathf.Min(scaledPenWidth, scaledPenHeight) * LerpDrawDamp;
            _uvRect.width = canvasWidth;
            _uvRect.height = canvasHeight;
            for (float i = 0; i < distance; i += lerpDamp)
            {
                var lDelta = i / distance;
                var lDifX = current.x - prev.x;
                var lDifY = current.y - prev.y;
                tempPos.x = prev.x + (lDifX * lDelta);
                tempPos.y = prev.y + (lDifY * lDelta);
                var rect = new Rect(tempPos.x - scaledPenWidth * 0.5f, tempPos.y - scaledPenHeight * 0.5f,
                    scaledPenWidth, scaledPenHeight);
                if (Intersect(ref _uvRect, ref rect))
                {
                    Graphics.DrawTexture(rect, penTexture);
                }
            }
        }

        void EndDraw()
        {
            // penColor = Random.ColorHSV();
            // if(Application.platform == RuntimePlatform.Android){
            // RenderTexture.active = renderTex;
            // Texture2D temp = new Texture2D((int) canvasWidth, (int) canvasHeight);
            // temp.ReadPixels(new Rect(0, 0, canvasWidth, canvasHeight), 0, 0);
            // temp.Apply();
            // RenderTexture.active = null;
            // Debug.Log($"setup new texture with width: {temp.width}, height: {temp.height}");
            //
            // int scaledWidth = (int) (canvasWidth * 0.05f);
            // int scaledHeight = (int) (canvasHeight * 0.05f);
            // TextureScale.Point(temp, scaledWidth, scaledHeight);
            // Debug.Log($"scaled down out-texture to w: {temp.width}, h: {temp.height}");

            // digitClassifier?.classify(scaledWidth, scaledHeight, temp.GetRawTextureData<sbyte>().ToArray());
            // }
        }

        public void ResetCanvas()
        {
            Graphics.SetRenderTarget(_renderTex);
            GL.Clear(true, true, new Color(0, 0, 0, 0));
            RenderTexture.active = null;
        }

        private bool Intersect(ref Rect a, ref Rect b)
        {
            bool c1 = a.xMin < b.xMax;
            bool c2 = a.xMax > b.xMin;
            bool c3 = a.yMin < b.yMax;
            bool c4 = a.yMax > b.yMin;
            return c1 && c2 && c3 && c4;
        }
    }

    public static class MaterialParams
    {
        public static readonly int Color = Shader.PropertyToID("_Color");
    }
}