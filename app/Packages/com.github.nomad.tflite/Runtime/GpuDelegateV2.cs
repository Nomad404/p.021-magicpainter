﻿#if UNITY_ANDROID && !UNITY_EDITOR

using System.Runtime.InteropServices;
using UnityEngine;
using TfLiteDelegate = System.IntPtr;

namespace TensorFlowLite
{
    public class GpuDelegateV2 : IBindableDelegate
    {
        public TfLiteDelegate Delegate { get; private set; }

        public static Options DefaultOptions => TfLiteGpuDelegateOptionsV2Default();

        public GpuDelegateV2()
        {
            Options options = DefaultOptions;
            Delegate = TfLiteGpuDelegateV2Create(ref options);
        }

        public GpuDelegateV2(Options options)
        {
            Delegate = TfLiteGpuDelegateV2Create(ref options);
        }

        public void Dispose()
        {
            TfLiteGpuDelegateV2Delete(Delegate);
            Delegate = TfLiteDelegate.Zero;
        }

        public bool BindBufferToInputTensor(Interpreter interpreter, int index, ComputeBuffer buffer)
        {
            uint bufferId = (uint) buffer.GetNativeBufferPtr().ToInt32();
            var status = TfLiteGpuDelegateV2BindInputBuffer(Delegate, index, bufferId);
            return status == Interpreter.Status.Ok;
        }

        public bool BindBufferToOutputTensor(Interpreter interpreter, int index, ComputeBuffer buffer)
        {
            uint bufferId = (uint) buffer.GetNativeBufferPtr().ToInt32();
            var status = TfLiteGpuDelegateV2BindOutputBuffer(Delegate, index, bufferId);
            return status == Interpreter.Status.Ok;
        }
        
        /// <summary>
        /// TfLiteGpuInferenceUsage
        /// Encapsulated compilation/runtime tradeoffs.
        /// </summary>
        public enum Usage
        {
            // Delegate will be used only once, therefore,
            // bootstrap/init time should be taken into account
            FastSingleAnswer = 0,

            // Prefer maximizing the throughput. Some delegate will
            // be used repeatedly on multiple inputs.
            SustainedSpeed = 1,
        }

        /// <summary>
        /// TfLiteGpuInferencePriority
        /// </summary>
        public enum InferencePriority
        {
            Auto = 0,
            MaxPrecision = 1,
            MinLatency = 2,
            MinMemoryUsage = 3,
        }

        /// <summary>
        /// TfLiteGpuExperimentalFlags
        /// </summary>
        [System.Flags]
        public enum ExperimentalFlags
        {
            None = 0,

            // Enables inference on quantized models with the delegate
            // NOTE: This enabled in TfLiteGpuDelegateOptions
            EnableQuant = 1 << 0,

            // Enforces execution with the provided backend
            ClOnly = 1 << 1,
            GlOnly = 1 << 2
        }

        /// <summary>
        /// The mirror of TfLiteGpuDelegateOptionsV2
        /// </summary>
        [StructLayout(LayoutKind.Sequential)]
        public struct Options
        {
            public int isPrecisionLossAllowed;
            public int inferencePreference;
            public int inferencePriority1;
            public int inferencePriority2;
            public int inferencePriority3;
            public long experimentalFlags;
            public int maxDelegatedPartitions;
        }

        #region External

        private const string TensorFlowLibraryGPU = "libtensorflowlite_gpu_delegate";

        [DllImport(TensorFlowLibraryGPU)]
        private static extern unsafe Options TfLiteGpuDelegateOptionsV2Default();

        [DllImport(TensorFlowLibraryGPU)]
        private static extern unsafe TfLiteDelegate TfLiteGpuDelegateV2Create(ref Options options);

        [DllImport(TensorFlowLibraryGPU)]
        private static extern unsafe void TfLiteGpuDelegateV2Delete(TfLiteDelegate gpuDelegate);

        [DllImport(TensorFlowLibraryGPU)]
        private static extern unsafe Interpreter.Status TfLiteGpuDelegateBindBufferToTensor(TfLiteDelegate gpuDelegate,
            uint buffer, int tensor_index);

        [DllImport(TensorFlowLibraryGPU)]
        private static extern unsafe Interpreter.Status TfLiteGpuDelegateV2BindInputBuffer(TfLiteDelegate gpuDelegate,
            int index, uint buffer);

        [DllImport(TensorFlowLibraryGPU)]
        private static extern unsafe Interpreter.Status TfLiteGpuDelegateV2BindOutputBuffer(TfLiteDelegate gpuDelegate,
            int index, uint buffer);

        #endregion
    }
}
#endif // UNITY_ANDROID && !UNITY_EDITOR