﻿using System.IO;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace TensorFlowLite.Utils
{
    public class FilePopupAttribute : PropertyAttribute
    {
        public string Regex;

        public FilePopupAttribute(string regex)
        {
            Regex = regex;
        }
    }

#if UNITY_EDITOR
    [CustomPropertyDrawer(typeof(FilePopupAttribute))]
    public class FilePopupDrawer : PropertyDrawer
    {
        private string[] _displayNames;
        private int _selectedIndex = -1;

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            if (property.propertyType != SerializedPropertyType.String)
            {
                Debug.LogError($"type: {property.propertyType} is not supported.");
                EditorGUI.LabelField(position, label.text, "Use FilePopup with string");
                return;
            }

            if (_displayNames == null)
            {
                string regex = (attribute as FilePopupAttribute)?.Regex;
                InitDisplayNames(regex);
            }

            if (_selectedIndex < 0)
            {
                _selectedIndex = FindSelectedIndex(property.stringValue);
            }

            EditorGUI.BeginProperty(position, label, property);

            _selectedIndex = EditorGUI.Popup(position, label.text, _selectedIndex, _displayNames);
            property.stringValue = _displayNames[_selectedIndex];
            
            EditorGUI.EndProperty();
        }

        void InitDisplayNames(string regex)
        {
            string[] fullPaths =
                Directory.GetFiles(Application.streamingAssetsPath, regex, SearchOption.AllDirectories);

            _displayNames = fullPaths.Select(fullPath =>
            {
                string path = fullPath.Replace(Application.streamingAssetsPath, "").Replace('\\', '/');
                if (path.StartsWith("/"))
                {
                    path = path.Substring(1);
                }

                return path;
            }).ToArray();
        }

        int FindSelectedIndex(string value)
        {
            for (int i = 0; i < _displayNames.Length; i++)
            {
                if (_displayNames[i] == value)
                {
                    return i;
                }
            }

            return 0;
        }
    }
#endif // UNITY_EDITOR
}