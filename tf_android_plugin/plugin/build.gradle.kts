plugins {
    id("com.android.library")
    kotlin("android")
    id("de.undercouch.download")
}

android {
    compileSdk  = 30
    buildToolsVersion = "30.0.3"

    defaultConfig {
        minSdk = 22
        targetSdk = 30

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        consumerProguardFiles("consumer-rules.pro")
    }

    androidResources {
        noCompress("tflite")
    }

    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
}

// import DownloadModels task
project.ext.set("RES_DIR", "$projectDir/src/main/res")

// Download default models; if you wish to use your own models then
// place them in the "assets" directory and comment out this line.
tasks {
    withType(org.jetbrains.kotlin.gradle.tasks.KotlinCompile::class).all {
        kotlinOptions {
            jvmTarget = "1.8"
        }
    }
    register("downloadModelFile", de.undercouch.gradle.tasks.download.Download::class){
        src("https://storage.googleapis.com/download.tensorflow.org/models/tflite/digit_classifier/mnist.tflite")
        dest(File("${project.ext.get("RES_DIR")}/raw/mnist.tflite"))
        overwrite(true)
    }
    whenTaskAdded {
        if(name.contains("assemble")) doLast {
            println("delete old plugin")
            delete("../../app/Assets/Plugins/Android/tf_unity.aar")
            println("copy and rename aar output")
            copy {
                from(file("$buildDir/outputs/aar"))
                include("*-release.aar")
                rename {
                    "tf_unity.aar"
                }
                into(file("../../app/Assets/Plugins/Android"))
            }
        }
    }
}

dependencies {
    implementation(fileTree(mapOf("dir" to "libs", "include" to listOf("*.jar"))))

    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk7:1.5.21")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.3.8")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-android:1.3.5")

    implementation("androidx.core:core-ktx:1.6.0")

    implementation("org.tensorflow:tensorflow-lite:2.4.0")
    implementation("org.tensorflow:tensorflow-lite-gpu:2.3.0")
    implementation("org.tensorflow:tensorflow-lite-support:0.1.0")

    testImplementation("junit:junit:4.13.2")
    androidTestImplementation("androidx.test.ext:junit:1.1.3")
    androidTestImplementation("androidx.test.espresso:espresso-core:3.4.0")
}
