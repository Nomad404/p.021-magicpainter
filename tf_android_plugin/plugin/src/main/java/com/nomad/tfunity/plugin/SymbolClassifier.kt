package com.nomad.tfunity.plugin

import android.content.Context
import android.graphics.Bitmap
import android.util.Log
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.tensorflow.lite.Interpreter
import java.io.FileInputStream
import java.io.IOException
import java.nio.ByteBuffer
import java.nio.ByteOrder
import java.nio.channels.FileChannel

class SymbolClassifier(private val context: Context) {

    companion object {
        private val TAG = SymbolClassifier::class.simpleName
        private const val FLOAT_TYPE_SIZE = 4
        private const val PIXEL_SIZE = 1
        private const val OUTPUT_CLASSES_COUNT = 34
    }

    private var interpreter: Interpreter? = null
    var isInitialized = false
        private set

    private var inputImageWidth: Int = 0 // will be inferred from TF Lite model
    private var inputImageHeight: Int = 0 // will be inferred from TF Lite model
    private var modelInputSize: Int = 0 // will be inferred from TF Lite model

    fun initialize() {
        Log.i(TAG, "initializing classifier")
        GlobalScope.launch {
            initializeInterpreter()
        }
    }

    @Throws(IOException::class)
    private suspend fun initializeInterpreter() {
        Log.i(TAG, "initializing interpreter")
        // Load the TF Lite model
        val model = loadModelFile()

        // Initialize TF Lite Interpreter with NNAPI enabled
        val options = Interpreter.Options()
        options.setUseNNAPI(true)
        val interpreter = Interpreter(model, options)

        // Read input shape from model file
        val inputShape = interpreter.getInputTensor(0).shape()
        inputImageWidth = inputShape[1]
        inputImageHeight = inputShape[2]
        modelInputSize = FLOAT_TYPE_SIZE * inputImageWidth * inputImageHeight * PIXEL_SIZE

        // Finish interpreter initialization
        this.interpreter = interpreter
        isInitialized = true
        Log.d(TAG, "Initialized TFLite interpreter.")
    }

    @Throws(IOException::class)
    private suspend fun loadModelFile(): ByteBuffer {
        Log.i(TAG, "loading model file")
        return withContext(Dispatchers.IO) {
            val fileDescriptor = context.resources.openRawResourceFd(R.raw.sketches)
            val inputStream = FileInputStream(fileDescriptor.fileDescriptor)
            val fileChannel = inputStream.channel
            val startOffset = fileDescriptor.startOffset
            val declaredLength = fileDescriptor.declaredLength
            fileChannel.map(
                FileChannel.MapMode.READ_ONLY,
                startOffset,
                declaredLength
            )
        }
    }

    private suspend fun classify(bitmap: Bitmap): PredictionResult {
        Log.i(TAG, "classify bitmap")
        return withContext(Dispatchers.IO) {
            if (!isInitialized) {
                throw IllegalStateException("TF Lite Interpreter is not initialized yet.")
            }

            // Preprocessing: resize the input
            var startTime: Long = System.nanoTime()
            Log.d(TAG, "old size: ${bitmap.width}x${bitmap.height}")
            val resizedImage =
                Bitmap.createScaledBitmap(bitmap, inputImageWidth, inputImageHeight, true)
            Log.d(TAG, "new size: ${resizedImage.width}x${resizedImage.height}")
            val byteBuffer = convertBitmapToByteBuffer(resizedImage)
            var elapsedTime = (System.nanoTime() - startTime) / 1000000
            Log.d(TAG, "Preprocessing time = " + elapsedTime + "ms")

            startTime = System.nanoTime()
            val result = Array(1) { FloatArray(OUTPUT_CLASSES_COUNT) }
            interpreter?.run(byteBuffer, result)
            elapsedTime = (System.nanoTime() - startTime) / 1000000
            Log.d(TAG, "Inference time = " + elapsedTime + "ms")

            result[0].toPredictionResult()
        }
    }

    fun classifyAsync(bitmap: Bitmap, callback: (PredictionResult) -> Unit) {
        GlobalScope.launch {
            val res = classify(bitmap)
            Log.i(TAG, "classify res: $res")
            withContext(Dispatchers.Main) {
                callback(res)
            }
        }
    }

    fun close() {
        GlobalScope.launch {
            interpreter?.close()
            Log.d(TAG, "Closed TFLite interpreter.")
        }
    }

    private fun convertBitmapToByteBuffer(bitmap: Bitmap): ByteBuffer {
        val byteBuffer = ByteBuffer.allocateDirect(modelInputSize)
        byteBuffer.order(ByteOrder.nativeOrder())

        val pixels = IntArray(inputImageWidth * inputImageHeight)
        bitmap.getPixels(pixels, 0, bitmap.width, 0, 0, bitmap.width, bitmap.height)

        for (pixelValue in pixels) {
            val r = (pixelValue shr 16 and 0xFF)
            val g = (pixelValue shr 8 and 0xFF)
            val b = (pixelValue and 0xFF)

            // Convert RGB to grayscale and normalize pixel value to [0..1]
            val normalizedPixelValue = (r + g + b) / 3.0f / 255.0f
            byteBuffer.putFloat(normalizedPixelValue)
        }

        return byteBuffer
    }

    private fun FloatArray.toPredictionResult(): PredictionResult {
        val maxIndex = this.indices.maxByOrNull { this[it] } ?: -1
        return PredictionResult(maxIndex, this[maxIndex])
    }

    data class PredictionResult(val index: Int, val probability: Float)
}
