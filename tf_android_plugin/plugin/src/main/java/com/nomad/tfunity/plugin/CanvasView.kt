package com.nomad.tfunity.plugin

import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.util.Log
import android.view.MotionEvent
import android.view.View
import android.widget.Toast
import kotlin.math.abs

class CanvasView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr : Int = 0
) : View(context, attrs, defStyleAttr),
    View.OnTouchListener {

    companion object {
        private val TAG = CanvasView::class.simpleName
        private const val TOUCH_TOLERANCE = 4.0f
    }

    private var drawCanvas: Canvas = Canvas()
    private val colorsMap = HashMap<Path, Int>()
    private var line = Path()
    private var mX: Float = 0.0f
    private var mY: Float = 0.0f
    private val paint: Paint = Paint().apply {
        isAntiAlias = true // smooth edges of drawn line
        isDither = true
        color = Color.WHITE // default color is black
        style = Paint.Style.STROKE // solid line
        strokeJoin = Paint.Join.ROUND
        strokeWidth = 5f // set the default line width
        strokeCap = Paint.Cap.ROUND // rounded line ends
        alpha = 0x30
    }
    private val paths = ArrayList<Path>()
    private var selectedColor = Color.WHITE
    private val strokeMap = HashMap<Path, Float>()
    private val undonePaths = ArrayList<Path>()

    private var selectedStroke = 5.0f

    //Returns the entire canvas as a bitmap to the caller (alphalevel are included)
    val bitmap: Bitmap?
        get() {
            try {
                val b = Bitmap.createBitmap(this.width, this.height, Bitmap.Config.ARGB_8888)
                draw(Canvas(b))
                return b
            } catch (e: Exception) {
                Log.i(TAG, "$e")
            }
            return null
        }


    init {
        isFocusable = true
        isFocusableInTouchMode = true
    }

    //Drawing a line with the selected Color and Stroke width
    override fun onDraw(canvas: Canvas) {
        this.drawCanvas = canvas
        for (p in paths) {
            paint.color = colorsMap[p]!!
            paint.strokeWidth = strokeMap[p]!!
            canvas.drawPath(p, paint)
        }
        paint.color = selectedColor
        paint.strokeWidth = selectedStroke
        canvas.drawPath(line, paint)
    }

    //Responsible for moving actions in order to draw continously
    override fun onTouchEvent(event: MotionEvent): Boolean {
        event.run {
            when (event.action) {
                MotionEvent.ACTION_DOWN -> {
                    undonePaths.clear()
                    line.reset()
                    line.moveTo(x, y)
                    mX = x
                    mY = y
                }
                MotionEvent.ACTION_MOVE -> {
                    val dx = abs(x - mX)
                    val dy = abs(y - mY)
                    if (dx >= TOUCH_TOLERANCE || dy >= TOUCH_TOLERANCE) {
                        line.quadTo(mX, mY, (x + mX) / 2, (y + mY) / 2)
                        mX = x
                        mY = y
                    }
                }
                MotionEvent.ACTION_UP -> {
                    line.lineTo(mX, mY)
                    drawCanvas.drawPath(line, paint)// commit the path to our offscreen
                    paths.add(line)
                    strokeMap[line] = selectedStroke
                    colorsMap[line] = selectedColor
                    line = Path()
                }
            }
            invalidate()
        }
        return true
    }

    //Sets the painted lines Stroke width
    fun setStroke(paramFloat: Float) {
        selectedStroke = paramFloat
    }

    fun onClickUndo() {
        if (paths.size > 0) {
            undonePaths.add(paths.removeAt(paths.size - 1))
            invalidate()
        } else {
            Toast.makeText(context, "nothing more to undo", Toast.LENGTH_SHORT).show()
        }
    }

    // Sets the painted line's color
    fun setDrawingColor(color: Int) {
        selectedColor = color
    }

    //Required by the TouchListener interface
    override fun onTouch(v: View, event: MotionEvent): Boolean {
        return false
    }

    //Erases everything from the canvas
    fun clear() {
        undonePaths.clear()
        paths.clear()
        colorsMap.clear()
        strokeMap.clear()
        drawCanvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.MULTIPLY)
        invalidate()
    }
}