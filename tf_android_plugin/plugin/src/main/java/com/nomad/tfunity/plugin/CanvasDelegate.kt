package com.nomad.tfunity.plugin

import android.graphics.*
import kotlin.math.abs

class CanvasDelegate(
    width: Int,
    height: Int
) {

    companion object {
        private val TAG = CanvasView::class.simpleName
        private const val TOUCH_TOLERANCE = 4.0f
    }

    private val canvasBitmap: Bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
    private var drawCanvas: Canvas = Canvas(canvasBitmap)
    private val colorsMap = HashMap<Path, Int>()
    private var line = Path()
    private var mX: Float = 0.0f
    private var mY: Float = 0.0f
    private val paint: Paint = Paint().apply {
        isAntiAlias = true // smooth edges of drawn line
        isDither = true
        color = Color.WHITE // default color is black
        style = Paint.Style.STROKE // solid line
        strokeJoin = Paint.Join.ROUND
        strokeWidth = 5f // set the default line width
        strokeCap = Paint.Cap.ROUND // rounded line ends
        alpha = 0x30
    }
    private val paths = ArrayList<Path>()
    private var selectedColor = Color.WHITE
    private val strokeMap = HashMap<Path, Float>()
    private var selectedStroke = 70.0f

    val bitmap: Bitmap?
        get() {
            return canvasBitmap.copy(Bitmap.Config.ARGB_8888, false)
        }

    fun sendDrawEvent(x: Float, y: Float, type: DrawEventType) {
        when (type) {
            DrawEventType.DOWN -> {
                line.reset()
                line.moveTo(x, y)
                mX = x
                mY = y
            }
            DrawEventType.DRAG -> {
                val dx = abs(x - mX)
                val dy = abs(y - mY)
                if (dx >= TOUCH_TOLERANCE || dy >= TOUCH_TOLERANCE) {
                    line.quadTo(mX, mY, (x + mX) / 2, (y + mY) / 2)
                    mX = x
                    mY = y
                }
            }
            DrawEventType.UP -> {
                line.lineTo(mX, mY)
                drawCanvas.drawPath(line, paint)// commit the path to our offscreen
                paths.add(line)
                strokeMap[line] = selectedStroke
                colorsMap[line] = selectedColor
                line = Path()
            }
        }
        draw()
    }

    private fun draw() {
        for (p in paths) {
            paint.color = colorsMap[p]!!
            paint.strokeWidth = strokeMap[p]!!
            drawCanvas.drawPath(p, paint)
        }
        paint.color = selectedColor
        paint.strokeWidth = selectedStroke
        drawCanvas.drawPath(line, paint)
    }

    fun clearCanvas() {
        paths.clear()
        colorsMap.clear()
        strokeMap.clear()
        drawCanvas.drawColor(Color.BLACK, PorterDuff.Mode.MULTIPLY)
    }

    enum class DrawEventType(val value: Int) {
        DOWN(0),
        DRAG(1),
        UP(2);

        companion object {
            private val map = values().associateBy(DrawEventType::value)
            fun fromInt(type: Int) = map[type]
        }
    }
}