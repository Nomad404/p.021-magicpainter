package com.nomad.tfunity.plugin

import android.content.Context
import android.util.Log
import com.nomad.tfunity.plugin.CanvasDelegate.DrawEventType

class TFAgent(
    context: Context,
    private val externalListener : Listener? = null
) {
    companion object {
        private val TAG = TFAgent::class.simpleName
    }

    private val symbolClassifier = SymbolClassifier(context)
    private lateinit var canvasDelegate: CanvasDelegate

    fun initialize(initialWidth: Int, initialHeight: Int) {
        symbolClassifier.initialize()
        canvasDelegate = CanvasDelegate(initialWidth, initialHeight)
    }

    fun sendCanvasEvent(x: Float, y: Float, code: Int) {
        DrawEventType.fromInt(code)?.also { type ->
            canvasDelegate.sendDrawEvent(x, y, type)
        }
    }

    fun classifyDrawing() {
        canvasDelegate.bitmap?.also { bitmap ->
            if (symbolClassifier.isInitialized) {
                symbolClassifier.classifyAsync(bitmap){ res ->
                    externalListener?.onClassifyFinished(res.index, res.probability)
                }
            } else {
                Log.i(TAG, "${SymbolClassifier::class.simpleName} is not initilaized")
            }
        }
    }

    fun resetCanvas() {
        canvasDelegate.clearCanvas()
    }

    interface Listener {
        fun onClassifyFinished(index : Int, probability: Float)
    }
}