package com.nomad.tfunity.sample

import android.graphics.Color
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintSet
import com.nomad.tfunity.plugin.CanvasView
import com.nomad.tfunity.plugin.SymbolClassifier
import com.nomad.tfunity.sample.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    companion object {
        private val TAG = MainActivity::class.simpleName
    }

    private lateinit var canvasView: CanvasView
    private var digitClassifier = SymbolClassifier(this)

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        canvasView = CanvasView(this).apply {
            id = View.generateViewId()
            layoutParams = ConstraintLayout.LayoutParams(1080, 1080)
        }

        // Setup view instances
        canvasView.setStroke(70.0f)
        canvasView.setDrawingColor(Color.WHITE)
        canvasView.setBackgroundColor(Color.BLACK)

        binding.clMainContainer.addView(canvasView)
        ConstraintSet().apply {
            clone(binding.clMainContainer)
            connect(canvasView.id, ConstraintSet.TOP, ConstraintSet.PARENT_ID, ConstraintSet.TOP)
            connect(
                canvasView.id,
                ConstraintSet.START,
                ConstraintSet.PARENT_ID,
                ConstraintSet.START
            )
            connect(canvasView.id, ConstraintSet.END, ConstraintSet.PARENT_ID, ConstraintSet.END)
            clear(binding.divider.id, ConstraintSet.BOTTOM)
            connect(binding.divider.id, ConstraintSet.TOP, canvasView.id, ConstraintSet.BOTTOM)
        }.applyTo(binding.clMainContainer)

        // Setup clear drawing button
        binding.clearButton.setOnClickListener {
            canvasView.clear()
            binding.predictionText.text = getString(R.string.prediction_text_placeholder)
        }

        binding.classifyButton.setOnClickListener {
            classifyDrawing()
        }

        // Setup digit classifier
        digitClassifier.initialize()
    }

    private fun classifyDrawing() {
        canvasView.bitmap?.also { bitmap ->
            if (digitClassifier.isInitialized) {
                digitClassifier
                    .classifyAsync(bitmap) { res ->
                        Toast.makeText(
                            this,
                            "symbol-num: ${res.index}, chance: ${res.probability}",
                            Toast.LENGTH_LONG
                        )
                            .show()
                    }
            }
        }
    }
}
